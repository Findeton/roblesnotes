+++
categories = ["about"]
comments = false
date = "2773-01-17T12:59:13-04:00"
publishDate = "2019-01-18T12:59:13-04:00"
draft = false
slug = ""
tags = ["about"]
title = "About"
showcomments = false
showpagemeta = false

+++

Felix Robles is a person from Spain, born in the year AUC 2739 (1986 AD). He studied Telecommunications Engineering in Sevilla, and is currently working as a Product Engineer Engineer @GenerationHome in London.

Contact me at: felix AT wupi DOT io
