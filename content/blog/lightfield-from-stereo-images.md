+++
categories = ["light-field", "video", "wupi"]
comments = true
date = "2773-12-27T18:00:13-00:00"
publishDate = "2020-12-27T18:00:13-00:00"
draft = false
showpagemeta = true
showcomments = true
slug = ""
tags = ["light-field", "video", "wupi"]
title = "Light-field videos: Part II. Google's Stereo Magnification"
description = "Reproducing Google's Stereo Magnification project with PyTorch"

+++

Lately I've been trying to catch up with the latest developments in light-fields. My first project has been trying to understand Stereo Magnification: Learning View Synthesis using Multiplane Images from Google (2018). [Now](https://twitter.com/realFelixRobles/status/1343644292447932417) I've created [a Colab](https://colab.research.google.com/drive/12ILZ-I-pdm0QuJt9HktFf7RlWr5MudrZ?usp=sharing) that uses PyTorch and trains a model at low-res in less than 15 minutes using a small subset of the Real Estate 10k dataset. I also use DeepView's viewer (inside the Colab) to show the results.
