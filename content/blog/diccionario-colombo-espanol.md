+++
categories =  ["colombianadas", "spanish", "diccionario", "dispar"]
comments = true
date = "2774-05-31T12:59:13-04:00"
publishDate = "2021-05-31T12:59:13-04:00"
draft = false
showpagemeta = true
showcomments = true
slug = ""
tags = ["colombianadas", "spanish", "diccionario", "dispar"]
title = "Colombianadas"
description = "Diccionario colombo-español"

+++

Como español casado con una colombiana, he ido aprendiendo con el tiempo multitud de palabras colombianas que no se usan en España.
No he podido resistirme a hacer un diccionario para traducir "del colombiano al español" (o viceversa), habiendo llegado a más de 100 palabras y frases comunes de Colombia.

Espero que esto ayude a los muchos españoles y colombianos a entenderse mejor y que provoque alguna carcajada general al leerlo (¡fotojapón, te estoy señalando!). ¡Pongan en los comentarios las palabras que me faltan!

Ah y os animo a comprar medias (calcetines) de la marca de mi mujer, [Dispar](https://dispar.co), ¡que acaba de lanzar una flamante nueva colección!

| Colombiano             | Español                                       |
| ---------------------- | --------------------------------------------- |
| abono                  | entrada                                       |
| aburridor              | aburrido                                      |
| ahorita                | luego                                         |
| almorzar               | comer                                         |
| arete                  | pendiente                                     |
| avientar               | lanzar/matar                                  |
| bacano                 | guay                                          |
| banano                 | plátano                                       |
| berraco                | valiente                                      |
| bocadillo              | dulce de guayaba                              |
| bochinchero            | le gusta la pelea                             |
| botar                  | tirar/perder                                  |
| bravo                  | enfadado                                      |
| brasier                | sujetador                                     |
| buseta                 | autobús pequeño                               |
| buso                   | blusa                                         |
| cada 8 días            | cada semana                                   |
| camarones              | gambas                                        |
| caneca                 | basura (cesta)                                |
| cansón                 | pesado                                        |
| carne                  | ternera                                       |
| carritos chocones      | coches choque                                 |
| carro                  | coche                                         |
| chambonada             | ñapa                                          |
| chinita                | niña                                          |
| cobija                 | manta                                         |
| cola                   | culo                                          |
| comer                  | cenar                                         |
| con todos los juguetes | (algo muy completo)                           |
| controversial          | controvertido                                 |
| cuenta regresiva       | cuenta atrás                                  |
| cucos                  | bragas                                        |
| culicagao              | niñato                                        |
| dar papaya             | exponerse a que se aprovechen de uno          |
| días de días           | días y días                                   |
| eres una hueva         | qué tonta                                     |
| esa vieja              | esa mujer                                     |
| espicharlo             | apretujarlo                                   |
| estoy muy arrugada     | tengo muchas arrugas                          |
| estar en la olla       | estar terriblemente mal de salud/dinero/ánimo |
| fastidioso             | pesado                                        |
| felicitaciones         | felicidades                                   |
| fotojapón              | fotomatón                                     |
| friolento              | friolero                                      |
| galgerías              | chucherías                                    |
| gomelo                 | pijo                                          |
| gorrero                | gorrón                                        |
| guayabo                | resaca                                        |
| guisa                  | choni                                         |
| halar                  | tirar/jalar                                   |
| intenso                | pesado                                        |
| inversionista          | inversor                                      |
| jueputa                | hijo de puta                                  |
| lagaña                 | legaña                                        |
| lambón                 | lameculos                                     |
| langosta               | bogavante                                     |
| lazo                   | comba                                         |
| loza                   | platos                                        |
| maíz pira              | palomitas                                     |
| mal hecho              | ñapa                                          |
| mamerto                | perroflauta                                   |
| man                    | tío                                           |
| manejar                | conducir                                      |
| maní                   | cacahuetes                                    |
| maricada               | mariconada                                    |
| marihuanero            | porrero                                       |
| mat                    | esterilla                                     |
| mecato                 | golosinas                                     |
| me prestas el celu?    | me pasas el móvil?                            |
| me provoca             | se me antoja                                  |
| media                  | calcetín                                      |
| mesero                 | camarero                                      |
| mijito                 | tío                                           |
| molestando             | bromeando                                     |
| mono                   | rubio                                         |
| moretón                | moratón                                       |
| morrongo/solapado      | falso                                         |
| motosear               | (cuando a la ropa le sale pelusa)             |
| mueco                  | mellado                                       |
| ñapa                   | (dame) un poco más                            |
| ñoño                   | empollón                                      |
| nubado                 | nublado                                       |
| orinar                 | mear                                          |
| palanca                | enchufe                                       |
| pan tajado             | pan bimbo                                     |
| pandito                | bajo/superficial                              |
| pasito                 | suavemente                                    |
| parar                  | levantar                                      |
| parar bolas            | prestar atención                              |
| parcero/parce          | amigo                                         |
| parqués                | parchis                                       |
| pasto                  | césped                                        |
| patilla                | sandía                                        |
| pega                   | socarrat                                      |
| pegachento             | pegajoso                                      |
| pena                   | vergüenza                                     |
| piso                   | suelo                                         |
| plata                  | dinero                                        |
| plátano                | plátano macho/verde                           |
| plomero                | fontanero                                     |
| pola                   | cerveza                                       |
| polla                  | gallina                                       |
| polla                  | porra                                         |
| por abeja              | por listillo/a                                |
| prender                | encender                                      |
| quebrar                | matar                                         |
| remedar                | imitar                                        |
| reservación            | reserva                                       |
| rueda volante          | noria                                         |
| saco                   | jersey                                        |
| sapo                   | chismoso                                      |
| solapado               | falso                                         |
| sombrilla              | paraguas                                      |
| sumercé (su merced)    | usted                                         |
| tapabocas              | mascarilla                                    |
| tapete                 | alfombra                                      |
| tenaz                  | terrible                                      |
| tetero                 | biberón                                       |
| tinto                  | café                                          |
| torta                  | tarta                                         |
| trancón                | atasco                                        |
| trapero                | fregona                                       |
| traqueto               | cani                                          |
| trotar                 | correr                                        |
| tusa                   | despecho                                      |
| wifi /uaifai/          | wifi /guifi/                                  |
| ya                     | ahora (luego)                                 |
| zafar                  | soltar                                        |

Nota: Dependiendo de la región (tanto de España como de Colombia) se usarán unas u otras palabras, dejo a otros la tarea de recopilar el diccionario regional.
