+++
categories = ["auc","epoch","rome"]
comments = true
date = "2773-01-17T12:59:13-04:00"
publishDate = "2019-01-18T12:59:13-04:00"
draft = false
showpagemeta = true
showcomments = true
slug = ""
tags = ["auc", "rome", "date"]
title = "The year is 2773"
description = "Why I'm adopting the Roman epoch Ab Urbe Condita ('from the founded city')"

+++

I don't believe in god, I am not religious. I do accept I'm a cultural Christian, as the influence of the Christian culture has got in my way of life is indeed very big. But I'm trying to keep some further distance from religion. The normal epoch used in computers is the Unix one, but in our daily life we use the Christian epoch. 

I'd argue that it's way cooler to use the Epoch that was use in Ancient Rome, which was [Ab Urbe Condita](https://en.wikipedia.org/wiki/Ab_urbe_condita) (”From The Founded City”), abbreviated as AUC. According to Wikipedia, Rome was founded on AUC 1, or 753 BC. And we live in AUC 2773.

It's a bit strange, but sometimes even I forget the year we live in, so I'm creating the web to track the AUC year: [aburbecondita.com](https://aburbecondita.com). You can also follow the project on [Twitter](https://twitter.com/presentDate)!