+++
categories = ["projects"]
comments = false
showcomments = false
date = "2773-02-08T10:46:13-04:00"
publishDate = "2019-01-18T12:59:13-04:00"
draft = false
slug = ""
tags = ["auc", "lambowei", "wupi", "vr"]
title = "Projects"

showpagemeta = true
+++
This is a list of projects


# AUC
[Ab Urbe Condita](https://aburbecondita.com) is a webpage that shows the current year, using the Epoch from Ancient Rome: the year Rome was founded (AUC 2773 is 2020 AD). It also includes a calculator to easily discover things like the AUC year you were born.
# Donate eth button
I'm planning to add a donate Eth button to [AUC](https://aburbecondita.com), which could make for an easy side side-project. One day...
# Lambowei
[Lambowei](https://lambowei.com) is a controversial blockchain art project of mine. People bid for the space in a digital frame. It's a continuous auction and a bidder that outbids the previous winner will change the image in the frame, and at the same time the previous bidder will receive as reward part of the bid. This is implemented with an Ethereum contract written with the Vyper language.
# Gocrawl
A simple project to try learn some golang. A web crawler with two modes: simple HTTP GET fetching, and using chrome headless (not published yet).
# Wupi v1
[Wupi](https://wupi.io) is an Android app to ease casual communications. You install a tablet in your kitchen or communal space and your friends/family do it too. Then this shows in green the friends that are active, as the app detects movement. Calling is a single click, as the called party doesn't have to accept!
# Codename WUPI v2
Remove the server in the middle.
# Unnamed VR project
VR Lightfield project.
